module.exports = (component) => `import React from 'react'
import classes from './classes.module.css'

export const ${component} = ({}) => {
  return (
    <div className={classes.class1}>
      <p className={classes.class2}>${component}</p>
    </div>
  )
}
`
