const fs = require('fs')
const path = require('path')
const {template1, template2} = require('./templates')

let currentPath = path.resolve(__dirname, '..', 'src')
const cd = (newPath) => currentPath = path.resolve(currentPath, newPath)

const mkdir = (dirname) => {
  try {
    fs.mkdirSync(`${currentPath}/${dirname}`)
  } catch (error) {

  }
}

const createFile = (file, data) => {
  fs.writeFileSync(`${currentPath}/${file}`, data)
}

const appendFile = (file, data) => {
  fs.appendFileSync(`${currentPath}/${file}`, data)
}

const [componentName] = process.argv.slice(2)

if (componentName.trim().length !== 0) {
  mkdir('components')
  cd('components')
  mkdir(componentName)
  cd(componentName)
  createFile(`${componentName}.js`, template1(componentName))
  createFile('classes.module.css', template2())
  cd('..')
  cd('..')
  appendFile('App.js', `import {${componentName}} from './components/${componentName}/${componentName}'`)
}

